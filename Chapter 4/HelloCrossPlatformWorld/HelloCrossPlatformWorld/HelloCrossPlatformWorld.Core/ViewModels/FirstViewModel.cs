using MvvmCross.Core.ViewModels;
using Plugin.TextToSpeech.Abstractions;
using System.Windows.Input;

namespace HelloCrossPlatformWorld.Core.ViewModels
{
    public class FirstViewModel : MvxViewModel
    {
        private readonly ITextToSpeech _textToSpeech;
        private string _hello = "Hello MvvmCross";
        private string _name;

        public FirstViewModel(ITextToSpeech textToSpeech)
        {
            _textToSpeech = textToSpeech;
            SayHelloCommand = new MvxCommand(SayHello);
        }

        public string Hello
        {
            get { return _hello; }
            set { SetProperty(ref _hello, value); }
        }

        public ICommand SayHelloCommand { get; private set; }

        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private void SayHello()
        {
            _textToSpeech.Speak($"Hello {Name}");
        }
    }
}
