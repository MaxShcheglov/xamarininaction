using System;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using Plugin.TextToSpeech.Abstractions;

namespace HelloCrossPlatformWorld.Core.ViewModels
{
    public class FirstViewModel
        : MvxViewModel
    {
        readonly ITextToSpeech textToSpeech;

        public FirstViewModel(ITextToSpeech textToSpeech)
        {
            this.textToSpeech = textToSpeech;
            SayHelloCommand = new MvxCommand(SayHello);
        }

        public ICommand SayHelloCommand { get; private set; }

        void SayHello()
        {
            Task.Run(() => MakeLongWebServiceCall())
               .ContinueWith(t =>
               {
                   var e = t.Exception;
                   textToSpeech.Speak($"Hello {Name}");
                   return "";
               });
        }

        private void MakeLongWebServiceCall()
        {
            Task.Delay(TimeSpan.FromSeconds(5)).Wait();
            throw new Exception();
        }

        string name = "";
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }
    }
}
