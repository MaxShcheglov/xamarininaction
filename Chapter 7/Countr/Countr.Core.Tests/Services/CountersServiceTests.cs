﻿using Countr.Core.Models;
using Countr.Core.Repositories;
using Countr.Core.Services;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Countr.Core.Tests.Services
{
    [TestFixture]
    public class CountersServiceTests
    {
        ICountersService service;
        Mock<ICountersRepository> repo;

        [SetUp]
        public void SetUp()
        {
            repo = new Mock<ICountersRepository>();
            service = new CountersService(repo.Object);
        }

        [Test]
        public async Task IncrementCounter_IncrementsTheCounter()
        {
            var counter = new Counter();

            await service.IncrementCounter(counter);

            Assert.AreEqual(1, counter.Count);
        }

        [Test]
        public async Task IncrementCounter_SavesTheIncrementedCounter()
        {
            var counter = new Counter { Count = 0 };

            await service.IncrementCounter(counter);

            repo.Verify(r => r.Save(It.Is<Counter>(c => c.Count == 1)), Times.Once);
        }

        [Test]
        public async Task GetAllCounters_ReturnsAllCountersFromTheRepository()
        {
            var counters = new List<Counter>
            {
                new Counter { Name = "Counter1" },
                new Counter { Name = "Counter2" }
            };

            repo.Setup(r => r.GetAll()).ReturnsAsync(counters);

            var results = await service.GetAllCounters();

            CollectionAssert.AreEqual(results, counters);
        }

        [Test]
        public async Task DeleteCounter_DeletesTheCounter()
        {
            var counter = new Counter { Count = 0 };

            await service.DeleteCounter(counter);

            repo.Verify(r => r.Delete(counter), Times.Once);
        }

        [Test]
        public async Task AddNewCounter_AddsNewCounter()
        {
            var counter = new Counter { Count = 0 };

            await service.AddNewCounter("Counter1");

            repo.Verify(r => r.Save(It.Is<Counter>(c => c.Name == "Counter1")), Times.Once);
        }
    }
}
