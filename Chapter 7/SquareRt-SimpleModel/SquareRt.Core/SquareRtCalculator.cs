﻿using System;

namespace SquareRt.Core
{
    public class SquareRtCalculator : ISquareRtCalculator
    {
        public SquareRtCalculator()
        {
        }

        public double Calculate(double number) => Math.Sqrt(number);
    }
}
