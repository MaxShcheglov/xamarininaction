﻿using System.Threading.Tasks;

namespace SquareRt.Core
{
    public interface ISquareRtCalculator
    {
        Task<double> Calculate(double number);
    }
}